#ifndef API_WATCH_H
#define API_WATCH_H 1

#include "stm32f10x.h"
#include "api_types.h"
#include "configuration.h"
#include "api_lcd.h"

       void api_watch_write_lcd_frame(void);
static void hex_to_char(uint8_t *u8_hex, char *ch_value);
static void api_watch_get_time_string(char ch_time[]);

#endif


