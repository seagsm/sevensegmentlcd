/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "api_lcd.h"

/* Inverted code-table. */
static LCD_CHAR lcd_char_table[] =
{
            /*  A         B         C         D         E         F         G       COLON      DOT   */
/* 00 */    {STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH}, /* blank () 0x20 ASCII. */
/* 01 */    {STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH, STD_HIGH}, /* 0, 0x30 ASCII. */
/* 02 */    {STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH}, /* 1, 0x31 ASCII. */
/* 03 */    {STD_LOW,  STD_LOW,  STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_LOW,  STD_HIGH, STD_HIGH}, /* 2, 0x32 ASCII. */
/* 04 */    {STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH, STD_LOW,  STD_HIGH, STD_HIGH}, /* 3, 0x33 ASCII. */
/* 05 */    {STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH}, /* 4, 0x34 ASCII. */
/* 06 */    {STD_LOW,  STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH}, /* 5, 0x35 ASCII. */
/* 07 */    {STD_LOW,  STD_HIGH, STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH}, /* 6, 0x36 ASCII. */
/* 08 */    {STD_LOW,  STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH}, /* 7, 0x37 ASCII. */
/* 09 */    {STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH}, /* 8, 0x38 ASCII. */
/* 10 */    {STD_LOW,  STD_LOW,  STD_LOW,  STD_LOW,  STD_HIGH, STD_LOW,  STD_LOW,  STD_HIGH, STD_HIGH}, /* 9, 0x39 ASCII. */
/* 11 */    {STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_LOW},  /* DOT, 0x2E ASCII. */
/* 12 */    {STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_HIGH, STD_LOW,  STD_HIGH}  /* COLON, 0x3A ASCII. */
};

/* LCD round buffer. It save all current elements. */
static LCD_CHAR lcd_mem_round_buffer[MAX_DISPLAY_POSITION];

/* LCD round buffer current index of head. */
static uint16_t u16_lcd_mem_buff_index = 0U;

/* Function clear LCD memmory buffer. */
void api_lcd_mem_round_buffer_init(void)
{
    uint16_t u16_index = 0U;
    while(u16_index < MAX_DISPLAY_POSITION)
    {
        lcd_mem_round_buffer[u16_index].segment_a     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_b     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_c     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_d     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_e     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_f     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_g     = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_colon = SEGMENT_OFF;
        lcd_mem_round_buffer[u16_index].segment_dot   = SEGMENT_OFF;
        u16_index++;
    }
}

/* Function get buffer head index. */
static void api_lcd_get_mem_buff_index(uint16_t *u16_value)
{
    *u16_value = u16_lcd_mem_buff_index;
}

/* Function set buffer head index. */
static void api_lcd_set_mem_buff_index(uint16_t u16_value)
{
    /* Check, input value should be less then max index of mem buffer. */
    if(u16_value <= MAX_DISPLAY_POSITION)
    {
        u16_lcd_mem_buff_index = u16_value;
    }
}

/* Function reset buffer head index. */
static void api_lcd_reset_mem_buff_index(void)
{
    u16_lcd_mem_buff_index = 0U;
}

/*
    Function read currend char footprint from LCD mem buffer.
    If u8_parameter is SHIFT_ON function will shift index to next position.
    If u8_parameter is SHIFT_OFF function just read last char footprint.
*/
void api_lcd_read_next_buff_element(LCD_CHAR *lc_value, uint8_t u8_parameter)
{
    uint16_t u16_index = 0U;

    api_lcd_get_mem_buff_index(&u16_index);

    if(u8_parameter == SHIFT_OFF)
    {   /* Set index to last head position. */
        if(u16_index == 0U)
        {
            u16_index = (MAX_DISPLAY_POSITION - 1U);
        }
        else
        {
            u16_index--;
        }
    }

    api_lcd_char_copy(&lcd_mem_round_buffer[u16_index], lc_value);

    if(u8_parameter == SHIFT_ON)
    {
        u16_index++;
        if(u16_index >= MAX_DISPLAY_POSITION)
        {
            api_lcd_reset_mem_buff_index();
        }
        else
        {
            api_lcd_set_mem_buff_index(u16_index);
        }
    }
}


/*
    Function write char footprint to LCD mem buffer.
    If u8_parameter is SHIFT_ON function will shift index to next position.
    If u8_parameter is SHIFT_OFF function just write char footprint to last head position.
    It is neccessary for writ DOT or COLON to same  characters possition.
*/
void api_lcd_write_next_buff_element(LCD_CHAR *lc_value, uint8_t u8_parameter)
{
    uint16_t u16_index = 0U;

    api_lcd_get_mem_buff_index(&u16_index);

    if(u8_parameter == SHIFT_OFF)
    {   /* Set index to last head position. */
        if(u16_index == 0U)
        {
            u16_index = (MAX_DISPLAY_POSITION - 1U);
        }
        else
        {
            u16_index--;
        }
    }

    api_lcd_char_copy(lc_value, &lcd_mem_round_buffer[u16_index]);

    if(u8_parameter == SHIFT_ON)
    {
        u16_index++;
        if(u16_index >= MAX_DISPLAY_POSITION)
        {
            api_lcd_reset_mem_buff_index();
        }
        else
        {
            api_lcd_set_mem_buff_index(u16_index);
        }
    }
}

/* Function convert char value to char footprint. */
void api_lcd_convert_char_to_lcd_char(char c_value, LCD_CHAR *lc_value)
{
    uint8_t u8_char = (uint8_t)c_value;
    uint8_t u8_index = 0U;

    /* In this case is better to use "switch". */
    switch(u8_char)
    {
        case 0x20:
            api_lcd_char_copy(&lcd_char_table[BLANK_TBL_INDEX], lc_value); /* BLANK_TBL_INDEX is position of 0x20(' ') ASCII character in our table.*/
        break;
        case 0x2E:
            api_lcd_char_copy(&lcd_char_table[DOT_TBL_INDEX], lc_value);   /* DOT_TBL_INDEX is position of 0x2E('.') ASCII character in our table.*/
            break;
        case 0x3A:
            api_lcd_char_copy(&lcd_char_table[COLON_TBL_INDEX], lc_value); /* COLON_TBL_INDEX is position of 0x3A (':') ASCII character in our table.*/
            break;
        case 0x30:
        case 0x31:
        case 0x32:
        case 0x33:
        case 0x34:
        case 0x35:
        case 0x36:
        case 0x37:
        case 0x38:
        case 0x39:
            u8_index = u8_char - DIGITS_TBL_SHIFT;
            api_lcd_char_copy(&lcd_char_table[u8_index], lc_value);        /* DIGITS_TBL_SHIFT shift character number              */
            break;                                                         /* to character position index in out character table. */
        default:
            api_lcd_char_copy(&lcd_char_table[BLANK_TBL_INDEX], lc_value); /* Let unrecognised character will be blank. */
            break;
    }
}

/* Function copy a value from  structure lc_from to structure lc_to. */
static void api_lcd_char_copy(LCD_CHAR *lc_from, LCD_CHAR *lc_to)
{
    lc_to->segment_a     = lc_from->segment_a;
    lc_to->segment_b     = lc_from->segment_b;
    lc_to->segment_c     = lc_from->segment_c;
    lc_to->segment_d     = lc_from->segment_d;
    lc_to->segment_e     = lc_from->segment_e;
    lc_to->segment_f     = lc_from->segment_f;
    lc_to->segment_g     = lc_from->segment_g;
    lc_to->segment_dot   = lc_from->segment_dot;
    lc_to->segment_colon = lc_from->segment_colon;
}

/* Write char footprint to LCD in position */
void api_lcd_write_char(LCD_CHAR *lc_value, uint8_t u8_position)
{
    api_lcd_off();
    Dio_WriteChannel(DIO_PA0, lc_value->segment_a);
    Dio_WriteChannel(DIO_PA1, lc_value->segment_b);
    Dio_WriteChannel(DIO_PA2, lc_value->segment_c);
    Dio_WriteChannel(DIO_PA3, lc_value->segment_d);
    Dio_WriteChannel(DIO_PA4, lc_value->segment_e);
    Dio_WriteChannel(DIO_PA5, lc_value->segment_f);
    Dio_WriteChannel(DIO_PA6, lc_value->segment_g);
    Dio_WriteChannel(DIO_PA7, lc_value->segment_colon);
    Dio_WriteChannel(DIO_PA8, lc_value->segment_dot);
    api_lcd_set_lcd_position(u8_position, STD_LOW);
}

/* Function ON/OFF LCD char position. STD_HIGH - off, STD_LOW - on */
static void api_lcd_set_lcd_position(uint8_t u8_position, dio_level_t dio_level)
{
    switch(u8_position)
    {
        case(0U):
            Dio_WriteChannel(DIO_PA9, dio_level);
        break;
        case(1U):
            Dio_WriteChannel(DIO_PA10, dio_level);
        break;
        case(2U):
            Dio_WriteChannel(DIO_PA11, dio_level);
        break;
        case(3U):
            Dio_WriteChannel(DIO_PA12, dio_level);
        break;
        case(4U):
            Dio_WriteChannel(DIO_PA13, dio_level);
        break;
        case(5U):
            Dio_WriteChannel(DIO_PA14, dio_level);
        break;
      default:
        break;
    }
}

/* Function ON/OFF LCD char position. STD_HIGH - off, STD_LOW - on */
void api_lcd_off(void)
{
    Dio_WriteChannel(DIO_PA9,  CHAR_OFF);
    Dio_WriteChannel(DIO_PA10, CHAR_OFF);
    Dio_WriteChannel(DIO_PA11, CHAR_OFF);
    Dio_WriteChannel(DIO_PA12, CHAR_OFF);
    Dio_WriteChannel(DIO_PA13, CHAR_OFF);
    Dio_WriteChannel(DIO_PA14, CHAR_OFF);
}















