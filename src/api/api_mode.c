/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "api_mode.h"

/* Value of current board mode. */
static MODE_STATE ms_mode_board_state = START_MODE;

/* Function return mode state. */
void api_mode_get_state(MODE_STATE *ms_value)
{
    *ms_value = ms_mode_board_state;
}

/* Function set mode state. */
static void api_mode_set_state(MODE_STATE ms_value)
{
    ms_mode_board_state = ms_value;
}

/*Function switch current board mode in depend on button state. */
void api_mode_switch(void)
{
           BUTTON_STATE bs_value_new  = BUTTON_UP;
    static BUTTON_STATE bs_value_old  = BUTTON_UP;
             MODE_STATE ms_mode_state = MODE_STRING;

    api_mode_get_state(&ms_mode_state);
    api_btn_get_state(&bs_value_new);

    switch(ms_mode_state)
    {
        case (MODE_STRING):
             if(bs_value_new == BUTTON_DOWN)
             {
                if(bs_value_new != bs_value_old)
                {   /* Switch mode to MODE_CLOCK */
                    api_mode_set_state(MODE_CLOCK);
                }
             }
            break;
        case (MODE_CLOCK):
             if(bs_value_new == BUTTON_DOWN)
             {
                if(bs_value_new != bs_value_old)
                {   /* Switch mode to MODE_STRING */
                    api_mode_set_state(MODE_STRING);
                }
             }
            break;
        default:
            break;
    }

    /* Save value of bs_value_new to use it next time. */
    bs_value_old = bs_value_new;
}



