/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "api_main.h"

static char inputSeq[] = "3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489";

/* Counting the number of characters in the string. */
static const uint16_t u16_size = (sizeof(inputSeq) / sizeof(inputSeq[0])) - 1U; /* -1U is because array is NULL terminated. */

/* Start point. */
void api_main(void)
{
    MODE_STATE ms_value;

    /* Check mode switch. */
    api_mode_switch();

    /* Get current mode. */
    api_mode_get_state(&ms_value);

    if(ms_value == MODE_STRING)
    {
        /*
            api_main_write_lcd_frame() has OsWait() inside.
            It took  ONE_TICK_WAIT * MAX_DISPLAY_POSITION  time.
        */
        api_main_write_lcd_frame();
        /*
            Each SHIFT_CHARACTER_TIME time new character added to LCD memory and
            "head index" shifted.
        */
        api_main_add_new_char_to_lcd_mem(inputSeq, u16_size, SHIFT_CHARACTER_TIME);
    }
    else if(ms_value == MODE_CLOCK)
    {   /* If mode_clock is, we print time on LCD. */
        api_watch_write_lcd_frame();
    }
    else
    {

    }
}

/*Function add new char to LCD mem buff each u16_shift_time time. */
static void api_main_add_new_char_to_lcd_mem(char ch_value[], uint16_t u16_size_value, uint16_t u16_shift_time)
{
           LCD_CHAR lc_value;
           uint16_t u16_counter     = 0U;
           char     ch_current_char = ' ';
    static uint16_t u16_shift_wait  = 0U;
    static uint16_t u16_seq_index   = 0U;
    SPEC_CHAR_STATE scs_flag        = NO_SPEC_CHAR;

    /* If input string end, add a separating symbol. */
    if(u16_seq_index >= u16_size_value)
    {
        ch_current_char = SYMBOL_SEPARATOR;
    }
    else
    {
        ch_current_char = ch_value[u16_seq_index];
    }

    /* Calc current counter value. */
    u16_counter = (u16_shift_time) / ((ONE_TICK_WAIT)*(MAX_DISPLAY_POSITION));

    /* If true, add new character to LCD mem buff. */
    if(u16_shift_wait >= u16_counter)
    {
        u16_shift_wait = 0U;

        /* Check special characters. */
        switch(ch_value[u16_seq_index])
        {
            case('.'):
                api_lcd_read_next_buff_element(&lc_value, SHIFT_OFF);
                if(lc_value.segment_dot == STD_HIGH)
                {
                    lc_value.segment_dot = STD_LOW;
                    api_lcd_write_next_buff_element(&lc_value, SHIFT_OFF);
                    scs_flag = DOT_ON;
                }
                break;
            case(':'):
                api_lcd_read_next_buff_element(&lc_value, SHIFT_OFF);
                if(lc_value.segment_colon == STD_HIGH)
                {
                    lc_value.segment_colon = STD_LOW;
                    api_lcd_write_next_buff_element(&lc_value, SHIFT_OFF);
                    scs_flag = COLON_ON;
                }
                break;
             default:
                break;
        }

        /* Write symbol to new position if neccessary. */
        switch(scs_flag)
        {
            case (DOT_ON):
            case (COLON_ON):
                break;
            default:
                /* Get char footprint from code-table. */
                api_lcd_convert_char_to_lcd_char(ch_current_char, &lc_value);
                /* Write new element to LCD mem buff. */
                api_lcd_write_next_buff_element(&lc_value, SHIFT_ON);
              break;
        }

        /*Turn round input buffer. */
        u16_seq_index++;
        if(u16_seq_index >= (u16_size_value + NUMBERS_OF_ADD_SYMBOLS)) /* increase index for separating symbols. */
        {
            u16_seq_index = 0U;
        }
    }
    else
    {
        u16_shift_wait++;
    }
}

/*
    Function read next element from LCD memoru buffer and write it to LCD.
    It do it for each position of LCD.
*/
static void api_main_write_lcd_frame(void)
{
    LCD_CHAR lc_value;
    uint8_t u8_lcd_index = 0U;

    while(u8_lcd_index < MAX_DISPLAY_POSITION)
    {
        api_lcd_read_next_buff_element(&lc_value, SHIFT_ON);
        /* Write char to LCD. */
        api_lcd_write_char(&lc_value, u8_lcd_index);
        u8_lcd_index++;
        OS_Wait(ONE_TICK_WAIT);

        /* Here we add BUTTON PRESS request. */
        api_btn_pressed_chek();

    }
}















