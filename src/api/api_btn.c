/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "api_btn.h"


static BUTTON_STATE bs_button_state = BUTTON_UP;

/* Function return button state. */
void api_btn_get_state(BUTTON_STATE *bs_value)
{
    *bs_value = bs_button_state;
}

/* Function set button state. */
static void api_btn_set_state(BUTTON_STATE bs_value)
{
    bs_button_state = bs_value;
}

/* Function check button state and switch it. */
void api_btn_pressed_chek(void)
{
    static uint32_t     u32_press_counter = 0U;
           dio_level_t  dio_level         = STD_HIGH;
           BUTTON_STATE bs_state          = BUTTON_UP;

    /* Get button level. */
    dio_level = Dio_ReadChannel(DIO_CLOCK_BTN);

    /* Get button_state state. */
    api_btn_get_state(&bs_state);

    switch(bs_state)
    {   /* Remove button pressing noise. */
        case (BUTTON_UP):
            /* Count pressed time. */
            if(dio_level == STD_LOW)
            {
                u32_press_counter++;
            }
            else
            {
                u32_press_counter = 0U;
            }
            /* If true, it means button pressed. */
            if(u32_press_counter >= BTN_PRESS_WAIT_TIME)
            {
                api_btn_set_state(BUTTON_DOWN);
            }
            break;
        case (BUTTON_DOWN):
            /* Count unpressed time. */
            if(dio_level == STD_HIGH)
            {
                u32_press_counter++;
            }
            else
            {
                u32_press_counter = 0U;
            }
            /* If true, it means button pressed. */
            if(u32_press_counter >= BTN_PRESS_WAIT_TIME)
            {
                api_btn_set_state(BUTTON_UP);
            }
            break;
      default:
            break;
    }
}





















