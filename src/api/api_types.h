#ifndef API_TYPES_H
#define API_TYPES_H 1

#include "stm32f10x.h"
#include "board_hal.h"

#define API_OK          0x0000U
#define API_ERROR       0x0001U

/* Structure describe 7-segments indicator. */
typedef struct
{
    dio_level_t segment_a;
    dio_level_t segment_b;
    dio_level_t segment_c;
    dio_level_t segment_d;
    dio_level_t segment_e;
    dio_level_t segment_f;
    dio_level_t segment_g;
    dio_level_t segment_colon;
    dio_level_t segment_dot;
}LCD_CHAR;

typedef enum
{
    NO_SPEC_CHAR,
    COLON_ON,
    DOT_ON
}SPEC_CHAR_STATE;

typedef enum
{
    BUTTON_DOWN,
    BUTTON_UP,
}BUTTON_STATE;

typedef enum
{
    MODE_STRING,
    MODE_CLOCK,
}MODE_STATE;














#endif