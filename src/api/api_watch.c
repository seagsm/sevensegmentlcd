/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "api_watch.h"


/* Function print curent time to LCD. */
void api_watch_write_lcd_frame(void)
{
    LCD_CHAR lc_value;
    uint8_t u8_lcd_index = 0U;
    char ch_time[6];

    /* Read current time from system and conwert in to char array. */
    api_watch_get_time_string(ch_time);

    u8_lcd_index = 0U;
    while(u8_lcd_index < MAX_DISPLAY_POSITION)
    {   /* Read suitable char footprint. */
        api_lcd_convert_char_to_lcd_char(ch_time[u8_lcd_index], &lc_value);
        /* Add SEPARATOR symbol if neccessary. */
        if((u8_lcd_index == 1U) || (u8_lcd_index == 3U))
        {
            COLON_SEPARATOR;
        }
        /* Write footprint to LCD. */
        api_lcd_write_char(&lc_value, u8_lcd_index);
        u8_lcd_index++;
        OS_Wait(ONE_TICK_WAIT);
        /* Check button. */
        api_btn_pressed_chek();
    }
}

/* Function return six elements array of current time. */
static void api_watch_get_time_string(char ch_time[])
{
    uint8_t u8_hour      = 0U;
    uint8_t u8_minute    = 0U;
    uint8_t u8_sec       = 0U;
    uint8_t u8_value     = 0U;

    /* functions with variable number of arguments shall not be used (MISRA C 2004 rule 16.1) */
    /* sprintf(ch_time,"%d", u8_sec); Just why I did not use sprintf(). */

    OS_Get_ActualTime(&u8_hour, &u8_minute, &u8_sec);
    /* HOURS */
    u8_value = u8_hour/10U;
    hex_to_char(&u8_value, &ch_time[0]);
    u8_value = u8_hour - u8_value * 10U;
    hex_to_char(&u8_value, &ch_time[1]);
    /* MINUTES */
    u8_value = u8_minute/10U;
    hex_to_char(&u8_value, &ch_time[2]);
    u8_value = u8_minute - u8_value * 10U;
    hex_to_char(&u8_value, &ch_time[3]);
    /* SECUNDS */
    u8_value = u8_sec/10U;
    hex_to_char(&u8_value, &ch_time[4]);
    u8_value = u8_sec - u8_value * 10U;
    hex_to_char(&u8_value, &ch_time[5]);
}

/* Function convert one digit in range from 0 to 9 to ASCII character. */
static void hex_to_char(uint8_t *u8_hex, char *ch_value)
{
    if(*u8_hex < 10U)
    {
        *ch_value = (char)(*u8_hex + 0x30U); /* 0x30U is code of "0" in ASCII table. */
    }
}


















