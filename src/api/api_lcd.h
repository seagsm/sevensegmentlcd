#ifndef API_LCD_H
#define API_LCD_H 1


#include "stm32f10x.h"
#include "api_types.h"
#include "configuration.h"
#include "api_btn.h"

#define DIGITS_TBL_SHIFT 0x2FU /* 0x2F shift character number to character position index in out character table. */
#define BLANK_TBL_INDEX  0x00U /* 0x00 is index of 0x20 character in our  character table. */
#define DOT_TBL_INDEX      11U /* DOT_TBL_INDEX is position of 0x2E ASCII character in our table.*/
#define COLON_TBL_INDEX    12U /* COLON_TBL_INDEX is position of 0x2E ASCII character in our table.*/

#define CHAR_ON         STD_LOW
#define CHAR_OFF        STD_HIGH
#define SEGMENT_ON      STD_LOW
#define SEGMENT_OFF     STD_HIGH

#define SHIFT_OFF       0U
#define SHIFT_ON        1U


       void api_lcd_off(void);
       void api_lcd_write_char(LCD_CHAR *lc_value, uint8_t u8_position);
static void api_lcd_set_lcd_position(uint8_t u8_position, dio_level_t dio_level);
       void api_lcd_convert_char_to_lcd_char(char c_value, LCD_CHAR *lc_value);
static void api_lcd_char_copy(LCD_CHAR *lc_from, LCD_CHAR *lc_to);
       void api_lcd_mem_round_buffer_init(void);


static void api_lcd_get_mem_buff_index(uint16_t *u16_value);
static void api_lcd_set_mem_buff_index(uint16_t  u16_value);
static void api_lcd_reset_mem_buff_index(void);
       void api_lcd_read_next_buff_element(LCD_CHAR *lc_value, uint8_t u8_parameter);
       void api_lcd_write_next_buff_element(LCD_CHAR *lc_value, uint8_t u8_parameter);



#endif


