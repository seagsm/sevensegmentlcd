#ifndef API_BTN_H
#define API_BTN_H 1


#include "stm32f10x.h"
#include "board_hal.h"
#include "api_types.h"
#include "configuration.h"

       void api_btn_pressed_chek(void);
       void api_btn_get_state(BUTTON_STATE *bs_value);
static void api_btn_set_state(BUTTON_STATE bs_value);


#endif