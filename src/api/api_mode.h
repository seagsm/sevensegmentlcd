#ifndef API_MODE_H
#define API_MODE_H 1


#include "stm32f10x.h"
#include "board_hal.h"
#include "api_types.h"
#include "configuration.h"
#include "api_btn.h"

static void api_mode_set_state(MODE_STATE ms_value);
       void api_mode_get_state(MODE_STATE *ms_value);
       void api_mode_switch(void);



#endif