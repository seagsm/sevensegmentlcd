#ifndef API_MAIN_H
#define API_MAIN_H 1


#include "stm32f10x.h"
#include "api_types.h"
#include "api_lcd.h"
#include "configuration.h"
#include "api_mode.h"
#include "api_watch.h"

       void api_main(void);
static void api_main_write_lcd_frame(void);
static void api_main_add_new_char_to_lcd_mem(char ch_value[], uint16_t u16_size_value, uint16_t u16_shift_time);







#endif