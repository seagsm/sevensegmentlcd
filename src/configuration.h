#ifndef CONFIGURATION_H
#define CONFIGURATION_H 1


#define ONE_TICK_WAIT           2U              /* mS , Character "ON" time. */
#define MAX_DISPLAY_POSITION    6U              /* The number of digits of the display. max 6 position. */
#define SHIFT_CHARACTER_TIME    200U            /* Each SHIFT_CHARACTER_TIME time in mS character will be moved for one position. */
#define NUMBERS_OF_ADD_SYMBOLS  2U              /* Numbers of separating symbols of input string. */
#define SYMBOL_SEPARATOR        ' '             /* Blank is separating symbol of input string. */
#define BTN_PRESS_WAIT_TIME     50U             /* If button pressed during BTN_PRESS_WAIT_TIME  (mS), it is pressed. */
#define COLON_SEPARATOR         (lc_value.segment_dot = STD_LOW) /* Add SEPARATION symbol to character if neccessary. Should be COLON.  */
#define START_MODE              MODE_STRING     /* Start mode, can be MODE_STRING or MODE_CLOCK */

#endif

