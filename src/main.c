/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "main.h"

int main( void)
{
    /* Hardware initialisation. */
    board_init();

    /* API initialisation. */
    api_lcd_mem_round_buffer_init();

    /* Turn off LCD elements. */
    api_lcd_off();

    /* Start application. */
    while(1)
    {
        api_main();
    }

}

#ifdef  USE_FULL_ASSERT
uint32_t assert_failed(uint8_t* file, uint32_t line)
{
    /*  User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1U)
    {
        return(1U);
    }
}
#endif

