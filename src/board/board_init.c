/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "board_init.h"


/* Function make board initialisation. */
void board_init(void)
{
   board_peripheral_init();
   board_system_init();
}

/* Function make initialisation of peripheral devices. */
static void board_peripheral_init(void)
{
    /* GPIO initialisation. */
    v_gpio_init();
}

/* Function initialise and start system tick interrupt. */
static void board_system_init(void)
{
    /* Init system ticks. */
    gv_sys_tick_init();
    /* Interrupt enable. It is neccesary for function WAIT. */
    __enable_irq();
}

