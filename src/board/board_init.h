#ifndef BOARD_INIT_H
#define BOARD_INIT_H 1


#include "stm32f10x.h"
#include "gpio.h"
#include "sys_tick.h"


void board_init(void);
static void board_peripheral_init(void);
static void board_system_init(void);

#endif