#ifndef NVIC_H
#define NVIC_H 1


#include "stm32f10x.h"

/*  Define current priority and subpriority. */
/*  #define NVIC_PRIORITY_GPOUP NVIC_PriorityGroup_0 */
/*  #define NVIC_PRIORITY_GPOUP NVIC_PriorityGroup_1 */
/*  #define NVIC_PRIORITY_GPOUP NVIC_PriorityGroup_2 */
/*  #define NVIC_PRIORITY_GPOUP NVIC_PriorityGroup_3 */  /* preemption 0-7 , sub 0-1 */
  #define NVIC_PRIORITY_GPOUP NVIC_PriorityGroup_4       /* just priority 0-15*/
 /* It means that priority group can be 0-15, but subpriority only 0. */
 
 void NVIC_init(void);

#endif

