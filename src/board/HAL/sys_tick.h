#ifndef SYS_TICK_H
#define SYS_TICK_H 1


#include "stm32f10x.h"

void gv_sys_tick_init(void);
void SysTick_Handler(void);
void gv_sys_tick_delay(uint64_t u64_delay_time);
void gv_sys_tick_fast_delay( uint32_t u32_val);
     uint64_t gu64_read_system_time(void);


#endif
     