#ifndef GPIO_H
#define GPIO_H 1

#include "stm32f10x.h"
#include "gpio_config.h"

typedef struct
{
    GPIO_TypeDef*     gpio_port;
    uint16_t      u16_gpio_port_pin;
}GPIO_PIN;


       void v_gpio_init(void);
static void v_gpio_pin_init(
                                GPIO_TypeDef* gpio_board_port,
                                uint16_t u16_port_pin,
                                GPIOSpeed_TypeDef gpio_speed_pin_speed,
                                GPIOMode_TypeDef gpio_mode_pin_mode
                           );

#endif