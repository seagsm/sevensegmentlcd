#ifndef GPIO_CONFIG_H
#define GPIO_CONFIG_H 1

#include "stm32f10x.h"

/* Definitions of input and output GPIO pins names. */
#define DIO_PA0         GPIOA, GPIO_Pin_0
#define DIO_PA1         GPIOA, GPIO_Pin_1
#define DIO_PA2         GPIOA, GPIO_Pin_2
#define DIO_PA3         GPIOA, GPIO_Pin_3
#define DIO_PA4         GPIOA, GPIO_Pin_4
#define DIO_PA5         GPIOA, GPIO_Pin_5
#define DIO_PA6         GPIOA, GPIO_Pin_6
#define DIO_PA7         GPIOA, GPIO_Pin_7
#define DIO_PA8         GPIOA, GPIO_Pin_8
#define DIO_PA9         GPIOA, GPIO_Pin_9
#define DIO_PA10        GPIOA, GPIO_Pin_10
#define DIO_PA11        GPIOA, GPIO_Pin_11
#define DIO_PA12        GPIOA, GPIO_Pin_12
#define DIO_PA13        GPIOB, GPIO_Pin_13
#define DIO_PA14        GPIOB, GPIO_Pin_14

/* Output pin for system led. */
#define LED_0           GPIOB, GPIO_Pin_1

/* Input pin for button. */
#define DIO_CLOCK_BTN   GPIOB, GPIO_Pin_8

#endif