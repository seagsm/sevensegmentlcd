/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "gpio.h"

/* Array of GPIO pins for outputs.*/
static GPIO_PIN gpio_pin_out[] =
{
                            {DIO_PA0},      /* segment A. */
                            {DIO_PA1},      /* segment B. */
                            {DIO_PA2},      /* segment C. */
                            {DIO_PA3},      /* segment D. */
                            {DIO_PA4},      /* segment E. */
                            {DIO_PA5},      /* segment F. */
                            {DIO_PA6},      /* segment G. */
                            {DIO_PA7},      /* segment COLON.   */
                            {DIO_PA8},      /* segment DOT.     */
                            {DIO_PA9},      /* segment DIG.1    */
                            {DIO_PA10},     /* segment DIG.2    */
                            {DIO_PA11},     /* segment DIG.3    */
                            {DIO_PA12},     /* segment DIG.4    */
                            {DIO_PA13},     /* segment DIG.5    */
                            {DIO_PA14},     /* segment DIG.6    */
                            {LED_0}
};

/* Array of GPIO pins for inputs.*/
static GPIO_PIN gpio_pin_in[] =
{
                            {DIO_CLOCK_BTN}
};

/* This function should set direction and mode for all GPIO pins. */
void v_gpio_init(void)
{
    uint16_t u16_index = 0U;
    uint16_t u16_size  = 0U;

    /* Calculating how much elements out-pins array has. */
    u16_size = sizeof(gpio_pin_out)/sizeof(gpio_pin_out[0]);
    /* Setup all array pins to output direction. */
    while(u16_index < u16_size)
    {
        /* Setup pin to output. */
        v_gpio_pin_init(
                            gpio_pin_out[u16_index].gpio_port,
                            gpio_pin_out[u16_index].u16_gpio_port_pin,
                            GPIO_Speed_10MHz,GPIO_Mode_Out_PP
                       );

        /* Set output to HIGH. It is hardware LCD specific.  */
        GPIO_SetBits(     gpio_pin_out[u16_index].gpio_port,
                            gpio_pin_out[u16_index].u16_gpio_port_pin
                      );
        u16_index++;
    }

    /* Reset index to 0*/
    u16_index = 0U;
    /* Calculating how much elements in-pins array has. */
    u16_size = sizeof(gpio_pin_in)/sizeof(gpio_pin_in[0]);
    /* Setup all array pins to input direction. */
    while(u16_index < u16_size)
    {
        /* Setup pin to input with Z-state . */
        v_gpio_pin_init(
                            gpio_pin_in[u16_index].gpio_port,
                            gpio_pin_in[u16_index].u16_gpio_port_pin,
                            GPIO_Speed_2MHz, GPIO_Mode_IN_FLOATING
                       );
        u16_index++;
    }
}

/*
    This function is setting GPIO pin on required mode and direction.
    GPIO_Mode_AIN             � analog input;
    GPIO_Mode_IN_FLOATING     � digital input, Z-state;
    GPIO_Mode_IPD             � digital input, pull-down;
    GPIO_Mode_IPU             � digital input, pull-up;
    GPIO_Mode_Out_OD          � output open drain;
    GPIO_Mode_Out_PP          � output push-pull;
    GPIO_Mode_AF_OD           - alternate function open-drain;
    GPIO_Mode_AF_PP           - alternate function push-pull;
*/
static void v_gpio_pin_init(
                                GPIO_TypeDef *gpio_board_port,
                                uint16_t u16_port_pin,
                                GPIOSpeed_TypeDef gpio_speed_pin_speed,
                                GPIOMode_TypeDef gpio_mode_pin_mode
                              )
{
    /* Create structure for initialisation. */
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable PORT Peripheral clock. */
    if(gpio_board_port == GPIOA)
    {
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA , ENABLE);
    }
    else if(gpio_board_port == GPIOB)
    {
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB , ENABLE);
    }
    else if(gpio_board_port == GPIOC)
    {
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC , ENABLE);
    }
    else
    {

    }

    /* Configurate the GPIO pin structure. */
    GPIO_InitStructure.GPIO_Pin   = u16_port_pin;
    GPIO_InitStructure.GPIO_Speed = gpio_speed_pin_speed;
    GPIO_InitStructure.GPIO_Mode  = gpio_mode_pin_mode;

    /* Call GPIO init function. */
    GPIO_Init( gpio_board_port, &GPIO_InitStructure);
}
