#ifndef BOARD_HAL_H
#define BOARD_HAL_H 1



#include "stm32f10x.h"
#include "gpio.h"
#include "sys_tick.h"


typedef enum
{
    STD_HIGH,
    STD_LOW
}dio_level_t;

#define dio_channel_t   GPIO_PIN
#define SYS_LED         dio_get_system_leds(0U)

         void OS_Wait(uint32_t delayMs);
         void OS_Get_ActualTime(uint8_t *u8_hour,uint8_t *u8_minute, uint8_t *u8_sec);
         void Dio_WriteChannel(GPIO_TypeDef* gpio_port, uint16_t u16_gpio_port_pin, dio_level_t level);
  dio_level_t Dio_ReadChannel(GPIO_TypeDef* gpio_port, uint16_t u16_gpio_port_pin);

dio_channel_t dio_get_system_leds(uint16_t u16_index);


#endif
