/*
    project : Ricardo Pre-Interview Test
    date    : 12.oktober.2015
    author  : Vadym Volokitin
    e-mail  : volokitin_v_n@yahoo.com
    licence : LGPL-3
*/

#include "board_hal.h"

/* Initialisation of LEDs array. */
static dio_channel_t leds[] =
{
    {LED_0}
};

/* Function return port and pin IDs for board LEDs. */
dio_channel_t dio_get_system_leds(uint16_t u16_index)
{
    uint16_t u16_size = 0U;
    dio_channel_t dio_value;

    u16_size  = sizeof(leds)/sizeof(leds[0]);

    if(u16_index < u16_size)
    {
        dio_value = leds[u16_index];
    }
    else
    {
        dio_value = leds[0];
    }

    return(dio_value);
}

/* System delay in milliseconds. */
void OS_Wait(uint32_t delayMs)
{
    gv_sys_tick_delay((uint64_t)delayMs);
}

/* This function did not provide real time. Just emulation. */
void OS_Get_ActualTime(uint8_t *u8_hour,uint8_t *u8_minute, uint8_t *u8_sec)
{
    uint64_t u64_time   = 0U;
    uint64_t u64_hour   = 0U;
    uint64_t u64_minute = 0U;
    uint64_t u64_sec    = 0U;

    /* Get current system time. In mS. */
    u64_time = gu64_read_system_time();

    /* Convert time to sec. */
    u64_time = u64_time /1000U;
    /* Add some value to see all digits position on LCD. */
    u64_time = u64_time + 67320U;/* Just time shift for test. */

    /* Get hours. */
    u64_hour =(uint8_t) (u64_time/3600U);
    u64_time =  u64_time - (u64_hour * 3600U);
    /*Get minutes. */
    u64_minute = u64_time/60U;
    /* Get seconds. */
    u64_sec = u64_time - (u64_minute * 60U);

    /* Convert. */
    *u8_hour   = (uint8_t)u64_hour;
    *u8_minute = (uint8_t)u64_minute;
    *u8_sec    = (uint8_t)u64_sec;
}

/* Function return pin state. */
dio_level_t Dio_ReadChannel(GPIO_TypeDef* gpio_port, uint16_t u16_gpio_port_pin)
{
    dio_level_t dl_result    = STD_LOW;
    uint8_t     u8_pin_state = 0U;

    /* Read pin input state. */
    u8_pin_state =  GPIO_ReadInputDataBit(gpio_port, u16_gpio_port_pin);

    /* BUTTON CONNECTED THROUGH 10K TO GROUND. Hardware specific. */
    /* So, lets emulate normal connection, then BUTTON_PRESS is STD_LOW and BUTTON_UNPRESS is STD_HIGH. */
    switch(u8_pin_state)
    {
      case 0:
        dl_result = STD_HIGH;
        break;

      case 1:
        dl_result = STD_LOW;
        break;

      default:
        break;
    }
    return (dl_result);
}

/* Function write pin state. */
void Dio_WriteChannel(GPIO_TypeDef* gpio_port, uint16_t u16_gpio_port_pin, dio_level_t level)
{
    switch(level)
    {
        case STD_LOW:
            GPIO_ResetBits(gpio_port, u16_gpio_port_pin);
            break;
        case STD_HIGH:
            GPIO_SetBits(gpio_port, u16_gpio_port_pin);
            break;
        default:
            break;
    }
}





