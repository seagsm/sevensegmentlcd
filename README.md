# README #
This is project require IAR 7.4 for ARM. 
It was developed for STM32F103, MAPLE MINI board.
Delivered version 1.01

### What is this repository for? ###
* Ricardo Pre-Interview Test project.
* 1.01

### How do I get set up? ###
Just clone it and open by IAR 7.4
Read explanation in folder docs: 
api_doc.txt is described api functions. 
configuration_doc.txt - is described configuration parameters. 