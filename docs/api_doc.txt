Project structure.

Folder API :
Files:
        api_btn.c
        api_btn.h
This file contains functions for button processing.
    
    void api_btn_pressed_chek(void) 
    - scan buttons and switch it state. 
    
    void api_btn_get_state(BUTTON_STATE *bs_value) 
    - return button state value. Button state described by enum BUTTON_STATE (api_types.h) 
    static void api_btn_set_state(BUTTON_STATE bs_value) - set button state value.

        api_lcd.c
        api_lcd.h
This file contains characters LCD footprint for 7 segment display. LDC round memory buffer functionality.
LCD read/write footprint functions.

void api_lcd_off(void) 
    - function turn off display digits in all positions. Designed for 6 position display only.
    
void api_lcd_write_char(LCD_CHAR *lc_value, uint8_t u8_position) 
    - function light corresponding LCD digit segments digit in according with input footprints in setted position. 
    Function designed for 6 position display. Input footprint in LCD_CHAR *lc_value converted from char from footprint table, 
    input position in uint8_t u8_position. Input position range is 0 - 5 .
    
void api_lcd_convert_char_to_lcd_char(char c_value, LCD_CHAR *lc_value) 
    - function convert input char to LCD footprint. Support input characters ' ', '.', ':' and digits from 0 to 9. 
    For all another input function return BLANK (' ') footprint.  
    
void api_lcd_mem_round_buffer_init(void) 
    - function clear LCD round buffer memory.

static void api_lcd_char_copy(LCD_CHAR *lc_from, LCD_CHAR *lc_to) 
    - function copy one LCD footprint structure to another.
    
static void api_lcd_set_lcd_position(uint8_t u8_position, dio_level_t dio_level) 
    - function turn on/off LCD digit in setted position.
    Function designed maximum for 6 position. uint8_t u8_position range is from 0 to 5. Level is enum dio_level_t , can be high - STD_HIGH or low - STD_LOW.
    If STD_HIGH  digit will be "off", if STD_LOW - digit will be "on".   

void api_lcd_read_next_buff_element(LCD_CHAR *lc_value, uint8_t u8_parameter) 
    - if u8_parameter is SHIFT_ON , function return current pointed by head-index LCD footprint element from LCD round buffer and
    increase head-index. If u8_parameter is SHIFT_OFF, function return previous pointed by head-index LCD footprint element from LCD round buffer.
    void api_lcd_write_next_buff_element(LCD_CHAR *lc_value, uint8_t u8_parameter);

static void api_lcd_get_mem_buff_index(uint16_t *u16_value)
    - function return current LCD footprint memory buffer head-index.
    
static void api_lcd_set_mem_buff_index(uint16_t  u16_value)
    - function write current value of LCD footprint memory buffer head-index.
    
static void api_lcd_reset_mem_buff_index(void);
    - function reset (set to zero) LCD footprint memory buffer head-index.

        api_main.c
        api_main.h
This file contains static char inputSeq[]. api_main - entry function for the entire process. 
Refresh frame functionality for displaying string mode (MODE_STRING), write new character from input string to LCD.

    void api_main(void) - entry function. Should be called from infinity loop from main.c 

static void api_main_write_lcd_frame(void) 
    - function copy LCD memory round buffer to LCD segments and wait ONE_TICK_WAIT time for each digit. 
      Inside function called api_btn_pressed_chek() to scan button.  

static void api_main_add_new_char_to_lcd_mem(char ch_value[], uint16_t u16_size_value, uint16_t u16_shift_time)
    - function add new character from char ch_value[] string sequence array to LCD memory round buffer. After each u16_size_value will be added separating symbols. 
    (TODO_NEXT_ITERATION: for flexibility should be added current sequence index to input parameters.)

        api_mode.c
        api_mode.h
This file contains functionality for switching LDC displaying mode in depend on button state.
       
void api_mode_switch(void) 
    - function check button state and switch current LCD displaying mode.     
       
void api_mode_get_state(MODE_STATE *ms_value)
    - function return current LCD displaying mode like enum MODE_STATE. Can be MODE_STRING or MODE_CLOCK.
     
static void api_mode_set_state(MODE_STATE ms_value);
    - function set LCD displaying mode.               

        api_watch.c
        api_watch.h
This file contains functionality for displaying clock on LCD.   

void api_watch_write_lcd_frame(void)
    - function copy clock char buffer to LCD segments and wait ONE_TICK_WAIT time for each digit. 
      Inside function called api_btn_pressed_chek() to scan button. 

static void api_watch_get_time_string(char ch_time[])
    - function convert current system time to clock char buffer ch_time[].     
      
static void hex_to_char(uint8_t *u8_hex, char *ch_value);
    - function converting digit to ASCII code. 
    Support  digits from 0 to 9 only.    
        
        api_types.h
This file contains important api types.        


To start application, call api_lcd_mem_round_buffer_init() to initialize LCD memory buffer, 
switch LCD off by api_lcd_off() and call api_main() from main in infinite cycle.

main()
{
    /* Hardware initialisation. */
    board_init();

    /* API initialisation. */
    api_lcd_mem_round_buffer_init();

    /* Turn off LCD elements. */
    api_lcd_off();

    /* Start application. */
    while(1)
    {
        api_main();
    }
} 










